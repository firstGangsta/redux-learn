import React, { Component } from 'react';
import { connect } from 'react-redux'

import { buttonActions } from './actions';

class App extends Component {

  componentDidMount(){
      this.props.getData();
  }

  render() {
    const {state, add, decrease} = {...this.props};
    if (!state.loaded && !state.fail)
        return(<h1>Loading...</h1>);
        
    return (
      <div className="App">
        <button onClick={add}>+</button>
        <button onClick={decrease}>-</button>
        <div>{state.value}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
    return{
        state
    }
}

function mapDispatchToProps(dispatch){
    return{
        add: function() {
            dispatch(buttonActions.addAction())
        },
        decrease: function(){
            dispatch(buttonActions.decreaseAction())
        },
        getData: function() {
            buttonActions.getData(dispatch);
        }
    }
}

export const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);
