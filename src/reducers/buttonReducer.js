import { buttonConstants } from '../constants';

export const buttonReducer = (state = {loaded: false, fail: false, value: -1}, action) => {
    switch (action.type) {
        case buttonConstants.ADD:
            return Object.assign({}, state, {value: state.value + 1});
        case buttonConstants.DECREASE:
            return Object.assign({}, state, {value: state.value - 1});
        case buttonConstants.GET_DATA_REQUEST:
            return Object.assign({}, state, {loaded: false, fail: false});
        case buttonConstants.GET_DATA_SUCCESS:
            return Object.assign({}, state, {loaded: true, fail: false, value: action.data.value});
        case buttonConstants.GET_DATA_FAIL:
            return Object.assign({}, state, {loaded: false, fail: true, value: -1});
        default:
            return state;
    }
}
