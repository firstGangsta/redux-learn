import { buttonConstants } from "../constants";

function getData(dispatch) {
  const options = {
    headers: {
      "Content-Type": "application/json"
    }
  };

  dispatch(getDataRequest());

  setTimeout(() => {
      fetch("data.json", options)
        .then(notParsedData => notParsedData.json())
        .then(parsedData => dispatch(getDataSuccess(parsedData)))
        .catch(() => dispatch(getDataFail()));
  }, 5000)

}

function getDataSuccess(data) {
  return {
    type: "GET_DATA_SUCCESS",
    data: data
  };
}

function getDataFail() {
  return {
    type: "GET_DATA_FAIL"
  };
}

function getDataRequest() {
  return {
    type: "GET_DATA_REQUEST"
  };
}

function addAction() {
  return {
    type: buttonConstants.ADD
  };
}

function decreaseAction() {
  return {
    type: buttonConstants.DECREASE
  };
}

export const buttonActions = {
  addAction,
  decreaseAction,
  getData,
};
